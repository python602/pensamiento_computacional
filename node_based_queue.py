from double_linked_list import TwoWayNode


class Queue:
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    def enqueue(self, data):
        new_node = TwoWayNode(data)

        if self.head is None:
            self.head = new_node
            self.tail = self.head
        else:
            new_node.previous = self.tail
            self.tail.next = new_node
            self.tail = new_node

        self.count += 1

    def dequeue(self):
        current = self.head

        if self.count == 1:
            self.count -= 1
            self.head = None
            self.tail = None
        elif self.count > 1:
            self.head = self.head.next
            self.head.previous = None
            self.count -= 1

        return current.data


if __name__ == '__main__':
    food = Queue()
    food.enqueue('eggs')
    food.enqueue('ham')
    food.enqueue('spam')
    print(food.head.data)
    print()
    print(food.head.next.data)
    print()
    print(food.tail.data)
    print()
    print(food.tail.previous.data)
    print()
    print(food.count)
    print()
    print(food.dequeue())
    print()
    print(food.head.data)
