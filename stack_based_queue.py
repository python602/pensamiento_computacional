class Queue:
    def __init__(self):
        self.inbound_stack = []
        self.outbound_stack = []

    def enqueue(self, data):
        self.inbound_stack.append(data)

    def dequeue(self):
        if not self.outbound_stack:
            while self.inbound_stack:
                self.outbound_stack.append(self.inbound_stack.pop())

        return self.outbound_stack.pop()


if __name__ == '__main__':
    numbers = Queue()
    numbers.enqueue(5)
    numbers.enqueue(6)
    numbers.enqueue(7)
    print(numbers.inbound_stack)
    print()
    print(numbers.dequeue())
    print()
    print(numbers.inbound_stack)
    print()
    print(numbers.outbound_stack)
    print()
    print(numbers.dequeue())
    print()
    print(numbers.outbound_stack)
    print()
    print(numbers.dequeue())
    print()
    print(numbers.outbound_stack)
    print()
    print(numbers.inbound_stack)
    print(numbers.outbound_stack)
