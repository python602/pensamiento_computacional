from random import randint
from node_based_queue import Queue
from time import sleep


class Track:
    def __init__(self, title=None):
        self.title = title
        self.length = randint(3, 6)


class MediaPlayerQueue(Queue):
    def __init__(self):
        super(MediaPlayerQueue, self).__init__()

    def add_track(self, track):
        self.enqueue(track)

    def play(self):
        print(f'Count: {self.count}')

        while self.count > 0 and self.head is not None:
            current_track_node = self.dequeue()
            print(f'Now playing {current_track_node.title}')
            sleep(current_track_node.length)


if __name__ == '__main__':
    track1 = Track('New Mode')
    track2 = Track('Maybe So')
    track3 = Track('Willing To Trust')
    track4 = Track('Ignite The Love')
    track5 = Track('By Design')
    track6 = Track('In Love')

    media_player = MediaPlayerQueue()
    media_player.add_track(track1)
    media_player.add_track(track2)
    media_player.add_track(track3)
    media_player.add_track(track4)
    media_player.add_track(track5)
    media_player.add_track(track6)
    media_player.play()